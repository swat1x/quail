package ru.swat1x.quail.uitest.parameter;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

import java.util.Arrays;
import java.util.Random;

public class RandomValuesParameterResolver implements ParameterResolver {

  @Override
  public boolean supportsParameter(ParameterContext parameterContext,
                                   ExtensionContext extensionContext) throws ParameterResolutionException {
    return Arrays.stream(parameterContext.getParameter().getAnnotations())
            .anyMatch(annotation -> {
              return annotation.getClass().isAnnotationPresent(RandomString.class);
            });
  }

  @Override
  public Object resolveParameter(ParameterContext parameterContext,
                                 ExtensionContext extensionContext) throws ParameterResolutionException {
    var parameter = parameterContext.getParameter();

    var randomString = parameter.getAnnotation(RandomString.class);
    if (randomString != null) {
      return randomString.array()[new Random().nextInt(randomString.array().length)];
    }

    throw new IllegalArgumentException("Can't resolve random value for annotated parameter");
  }

}
