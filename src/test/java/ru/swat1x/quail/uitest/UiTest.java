package ru.swat1x.quail.uitest;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebElementCondition;
import com.codeborne.selenide.conditions.Enabled;
import com.codeborne.selenide.conditions.Exist;
import com.codeborne.selenide.conditions.OwnText;
import io.qameta.allure.Description;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import ru.swat1x.quail.uitest.page.SearchPage;
import ru.swat1x.quail.uitest.page.main.UnauthorizedPage;
import ru.swat1x.quail.uitest.parameter.RandomString;
import ru.swat1x.quail.uitest.parameter.RandomValuesParameterResolver;

import static com.codeborne.selenide.Selenide.*;

@ExtendWith(RandomValuesParameterResolver.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UiTest {

  @BeforeAll
  public static void configure() {
    Configuration.browser = "chrome";
    Configuration.baseUrl = "https://www.wildberries.ru";
  }

  @Test
  @Order(1)
  @Tag("ui")
  @DisplayName("Проверка отображения ключевых кнопок")
  @Description("Проверка отображения ключевых кнопок")
  public void basketButtonsDisplayTest() {
    open("/");
    var mainPage = new UnauthorizedPage();

    mainPage.addressesButton().shouldBe(enabledAndContains("Адреса"));
    mainPage.logInButton().shouldBe(enabledAndContains("Войти"));
    mainPage.basketButton().shouldBe(enabledAndContains("Корзина"));
  }

  @Test
  @Order(2)
  @Tag("ui")
  @DisplayName("Проверка работы поиска и наличия товаров")
  @Description("Проверка работы поиска и наличия товаров")
  public void searchTest(
          @RandomString(array = {"Красные мужские трусы",
                  "Черная женская сумочка",
                  "Стиральный порошок",
                  "Оранжевая рубашка"})
          String category) {
    open("/");
    var mainPage = new UnauthorizedPage();

    mainPage.searchInput().type(category);
    mainPage.searchInput().pressEnter();

    var searchPage = new SearchPage();

    Assertions.assertTrue(searchPage.isGaveGoods(), "Ни одного товара не найдено в категории: " + category);
  }

  private WebElementCondition[] enabledAndContains(String ownText) {
    return new WebElementCondition[]{new Exist(), new Enabled(), new OwnText(ownText)};
  }

}
