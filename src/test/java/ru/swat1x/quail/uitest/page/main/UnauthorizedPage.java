package ru.swat1x.quail.uitest.page.main;

import com.codeborne.selenide.SelenideElement;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import static com.codeborne.selenide.Selenide.$x;

@Getter
@Accessors(fluent = true)
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class UnauthorizedPage {

  SelenideElement addressesButton = $x("//div[@id='basketContent']/div[1]/a[1]");
  SelenideElement logInButton = $x("//div[@id='basketContent']/div[2]/a[1]");
  SelenideElement basketButton = $x("//div[@id='basketContent']/div[3]/a[1]");

  SelenideElement searchInput = $x("//input[@id='searchInput']");

}
