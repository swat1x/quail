package ru.swat1x.quail.uitest.page;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.conditions.Hidden;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import static com.codeborne.selenide.Selenide.$x;

@Getter
@Accessors(fluent = true)
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class SearchPage {

  SelenideElement goodsNotFoundDiv = $x("//div[@id='divGoodsNotFound']");
  SelenideElement goodsDiv = $x("//div[@id='divGoodsNotFound']/parent::div[1]");

  public boolean isGaveGoods() {
    return !goodsNotFoundDiv.isDisplayed();
  }

}
