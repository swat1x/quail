package ru.swat1x.quail.apitest;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.extension.ExtendWith;
import ru.swat1x.quail.apitest.entity.PersonModel;
import io.restassured.internal.mapping.GsonMapper;
import io.restassured.path.json.mapper.factory.DefaultGsonObjectMapperFactory;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import ru.swat1x.quail.apitest.restassured.RestAssuredSpecificationsExtension;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.CombinableMatcher.both;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;

@Slf4j
@ExtendWith(RestAssuredSpecificationsExtension.class)
public class LoginServiceTest {

  @Test
  @Tag("api")
  @Tag("regress")
  @DisplayName("Login with exists user's credentials test")
  void registerNewUser() {
    var person = new PersonModel(
            "eve.holt@reqres.in",
            "cityslicka"
    );
    var expectedToken = "QpwL5tke4Pnpja7X4";

    given()
            .body(person, new GsonMapper(new DefaultGsonObjectMapperFactory()))
            .log().method()
            .log().body()
            .when()
            .post("/login")
            .then()
            .log().all()
            .body(
                    "token",
                    both(notNullValue()).and(equalTo(expectedToken))
            );
  }

}
