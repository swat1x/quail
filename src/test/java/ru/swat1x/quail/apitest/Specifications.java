//package ru.swat1x.quail;
//
//import io.restassured.RestAssured;
//import io.restassured.builder.RequestSpecBuilder;
//import io.restassured.builder.ResponseSpecBuilder;
//import io.restassured.filter.Filter;
//import io.restassured.http.ContentType;
//import io.restassured.specification.RequestSpecification;
//import io.restassured.specification.ResponseSpecification;
//import org.hamcrest.Description;
//import org.hamcrest.Matcher;
//import org.hamcrest.collection.IsIn;
//import org.hamcrest.core.Is;
//
//import java.util.Arrays;
//import java.util.Collections;
//import java.util.HashSet;
//import java.util.function.Consumer;
//
//public class Specifications {
//
//
//
//  static void initializeSpecifications(RequestSpecification requestSpec, ResponseSpecification responseSpec) {
//    RestAssured.requestSpecification = requestSpec;
//    RestAssured.responseSpecification = responseSpec;
//  }
//
//  static RequestSpecification requestSpecification(String url, ContentType contentType) {
//    return new RequestSpecBuilder()
//            .setBaseUri(url)
//            .setContentType(contentType)
//            .build();
//  }
//
//  static RequestSpecification jsonRequestSpecification(String url) {
//    return requestSpecification(url, ContentType.JSON);
//  }
//
//  static RequestSpecification xmlRequestSpecification(String url) {
//    return requestSpecification(url, ContentType.XML);
//  }
//
//  static ResponseSpecification responseSpecification(Integer[] expectStatus, Consumer<ResponseSpecBuilder> additionalSpecs) {
//    var builder = new ResponseSpecBuilder().expectStatusCode(IsIn.in(expectStatus));
//    if (additionalSpecs != null) additionalSpecs.accept(builder);
//    return builder.build();
//  }
//
//  static ResponseSpecification responseSpecification(Integer... expectStatus) {
//    return responseSpecification(expectStatus, null);
//  }
//
//}
