package ru.swat1x.quail.apitest.restassured;

import io.restassured.http.ContentType;

public class RestAssuredSpecifications {

  public static final String BASE_URL = "https://reqres.in/api";

  public static final Integer[] ACCEPTED_STATUS_CODES = new Integer[] {
          200,
          201
  };

  public static final ContentType OUTBOUND_CONTENT_TYPE = ContentType.JSON;
  public static final ContentType INBOUND_CONTENT_TYPE = ContentType.JSON;

}
