package ru.swat1x.quail.apitest.restassured;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import org.hamcrest.collection.IsIn;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import static ru.swat1x.quail.apitest.restassured.RestAssuredSpecifications.*;

public class RestAssuredSpecificationsExtension implements BeforeEachCallback {

  private static final Integer[] acceptedCodes = new Integer[] {
          200,
          201
  };

  @Override
  public void beforeEach(ExtensionContext context) {
    RestAssured.requestSpecification = new RequestSpecBuilder()
            .setBaseUri(BASE_URL)
            .setContentType(OUTBOUND_CONTENT_TYPE)
            .build();
    RestAssured.responseSpecification = new ResponseSpecBuilder()
            .expectStatusCode(IsIn.in(ACCEPTED_STATUS_CODES))
            .expectContentType(INBOUND_CONTENT_TYPE)
            .build();
  }

}
